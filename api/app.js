const Express = require("express");
const Mongoose = require("mongoose");
const BodyParser = require("body-parser");
const cors = require('cors');
let app = Express();
Mongoose.connect('mongodb://localhost:27017/node-express-mongodb-server');
const avisSchema = new Mongoose.model("avis",{
    note: String,
    SIRET: String,
    text: String,
});

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
app.use(cors());

app.post("/avis", async (request, response) => {
    try {
        let avis = new avisSchema(request.body);
        let result = await avis.save();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});
app.get("/avis", async (request, response) => {
    try {
        var result = await avisSchema.find().sort([['_id', -1]]).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});
app.get("/avis/count1", async (request, response) => {
    try {
        avisSchema.count({}, function(err, count) {
            if (err) { return handleError(err) } //handle possible errors
            response.send({count: count});
        });
    } catch (error) {
        response.status(500).send(error);
    }
});
app.get("/avis/count2", async (request, response) => {
    try {
        avisSchema.count({text : ''}, function(err, count) {
            if (err) { return handleError(err) } //handle possible errors
            response.send({count: count});
        });
    } catch (error) {
        response.status(500).send(error);
    }
});

app.listen(3000, () => {
    console.log("Listening at :3000...");
});


