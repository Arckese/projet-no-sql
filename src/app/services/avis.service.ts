import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AvisService {

  constructor(public http: HttpClient) {}

  selectAllAvis() {
    return this.http.get('http://localhost:3000/avis');
  }
  selectCountAvis() {
    return this.http.get('http://localhost:3000/avis/count1');
  }
  selectCountAvisWithComs() {
    return this.http.get('http://localhost:3000/avis/count2');
  }

  addAvis(data) {
    let json;
    const xhr = new XMLHttpRequest();
    const url = 'http://localhost:3000/avis';
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4 && xhr.status === 200) {
        json = JSON.parse(xhr.responseText);
        console.log(json);
        return(json);
      }
    }
    const str = JSON.stringify(data);
    xhr.send(str);
  }
}
