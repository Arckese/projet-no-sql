import { Component, OnInit } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {AvisService} from '../../services/avis.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  homeForm;
  eventList;
  count1;
  count2;
  constructor(
      private formBuilder: FormBuilder,
      private avisService: AvisService
  ) {
    this.homeForm = this.formBuilder.group({
      note: '',
      SIRET: '',
      text: ''
    });
  }

  ngOnInit() {
    this.avisService.selectAllAvis().subscribe( (data) => {
      console.log(data);
      this.eventList = data;
    });
    this.avisService.selectCountAvis().subscribe( (data) => {
      this.count1 = data.count;
    });
    this.avisService.selectCountAvisWithComs().subscribe( (data) => {
      this.count2 = this.count1 - data.count;
    });
  }

  async onSubmit(customerData) {
    // Process checkout data here
    console.warn('Your order has been submitted', customerData);
    console.log(customerData);
    const json = {
      note: customerData.note,
      SIRET: customerData.SIRET,
      text: customerData.text
    };
    if (json.note === '' && json.SIRET.length !== 14) {
      alert('La note et le numéro SIRET ne sont pas remplis.');
    } else if (json.note === '' && json.SIRET.length === 14) {
      alert(`La note n'est pas remplie.`);
    } else if (json.note !== '' && json.SIRET.length !== 14 ) {
      alert(`Le numéro SIRET n'est pas rempli.`);
    } else {
      this.avisService.addAvis(json);
      alert(`L'avis a été enregistré`);
      this.avisService.selectAllAvis().subscribe( (data) => {
        console.log(data);
        this.eventList = data;
      });
      this.homeForm.reset();
    }

  }
}

